package Utils;

import Model.EnemyCharacter;
import Model.UserCharacter;

/**
 * Created by OPeratore on 10/03/2017.
 */
public interface CharacterFactory {


    UserCharacter createUserCharacter(int x, int y, String choice);

    EnemyCharacter createEnemyCharacter(int x, int y, String choice);


}
