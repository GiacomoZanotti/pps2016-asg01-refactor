package Utils;

import Model.*;

/**
 * Created by OPeratore on 10/03/2017.
 */
public class MyCharacterFactory implements CharacterFactory {


    @Override
    public UserCharacter createUserCharacter(int x, int y, String choice) {
        if(choice.equalsIgnoreCase("mario")){
            return new Mario(x,y);

        }
        return null;
    }

    @Override
    public  EnemyCharacter createEnemyCharacter(int x, int y,String choice) {
        if(choice.equalsIgnoreCase("turtle")){
            return new Turtle(x,y);
        }
        else if(choice.equalsIgnoreCase("mushroom")){
            return new Mushroom(x,y);

        }
        return null;
    }
}
