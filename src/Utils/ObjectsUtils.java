package Utils;

import Model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OPeratore on 15/03/2017.
 */
public class ObjectsUtils {

    private static List<GameObject> listOfGameObjects = new ArrayList<>();
    private static List<Coin> listOfPieces = new ArrayList<>();

    private static GameObject tunnel1 = new Tunnel(600, 230);
    private static GameObject tunnel2 = new Tunnel(1000, 230);
    private static GameObject tunnel3 = new Tunnel(1600, 230);
    private static GameObject tunnel4 = new Tunnel(1900, 230);
    private static GameObject tunnel5 = new Tunnel(2500, 230);
    private static GameObject tunnel6 = new Tunnel(3000, 230);
    private static GameObject tunnel7 = new Tunnel(3800, 230);
    private static GameObject tunnel8 = new Tunnel(4500, 230);

    private static GameObject block1 = new Block(400, 180);
    private static GameObject block2 = new Block(1200, 180);
    private static GameObject block3 = new Block(1270, 170);
    private static GameObject block4 = new Block(1340, 160);
    private static GameObject block5 = new Block(2000, 180);
    private static GameObject block6 = new Block(2600, 160);
    private static GameObject block7 = new Block(2650, 180);
    private static GameObject block8 = new Block(3500, 160);
    private static GameObject block9 = new Block(3550, 140);
    private static GameObject block10 = new Block(4000, 170);
    private static GameObject block11 = new Block(4200, 200);
    private static GameObject block12 = new Block(4300, 210);


    private static Coin piece1 = new Coin(402, 145);
    private static Coin piece2 = new Coin(1202, 140);
    private static Coin piece3 = new Coin(1272, 95);
    private static Coin piece4 = new Coin(1342, 40);
    private static Coin piece5 = new Coin(1650, 145);
    private static Coin piece6 = new Coin(2650, 145);
    private static Coin piece7 = new Coin(3000, 135);
    private static Coin piece8 = new Coin(3400, 125);
    private static Coin piece9 = new Coin(4200, 145);
    private static Coin piece10 = new Coin(4600, 40);

    public static List<GameObject> getListOfGameObjects() {


        listOfGameObjects.add(tunnel1);
        listOfGameObjects.add(tunnel2);
        listOfGameObjects.add(tunnel3);
        listOfGameObjects.add(tunnel4);
        listOfGameObjects.add(tunnel5);
        listOfGameObjects.add(tunnel6);
        listOfGameObjects.add(tunnel7);
        listOfGameObjects.add(tunnel8);

        listOfGameObjects.add(block1);
        listOfGameObjects.add(block2);
        listOfGameObjects.add(block3);
        listOfGameObjects.add(block4);
        listOfGameObjects.add(block5);
        listOfGameObjects.add(block6);
        listOfGameObjects.add(block7);
        listOfGameObjects.add(block8);
        listOfGameObjects.add(block9);
        listOfGameObjects.add(block10);
        listOfGameObjects.add(block11);
        listOfGameObjects.add(block12);
        return listOfGameObjects;
    }

    public static List<Coin> getListOfPieces() {
        listOfPieces.add(piece1);
        listOfPieces.add(piece2);
        listOfPieces.add(piece3);
        listOfPieces.add(piece4);
        listOfPieces.add(piece5);
        listOfPieces.add(piece6);
        listOfPieces.add(piece7);
        listOfPieces.add(piece8);
        listOfPieces.add(piece9);
        listOfPieces.add(piece10);
        return listOfPieces;
    }

    private ObjectsUtils() {
    }

}
