package Controller;

import Model.*;
import View.Audio;
import View.Platform;
import Utils.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by OPeratore on 19/03/2017.
 */
public class UserCharacterController {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;

    private UserCharacter mario;
    private int jumpingExtent;
    private static final int JUMPING_LIMIT = 42;
    private List<Coin> coins = new ArrayList<>(ObjectsUtils.getListOfPieces());

    public UserCharacterController() {
        CharacterFactory characterFactory = new MyCharacterFactory();
        mario = characterFactory.createUserCharacter(300, 245, "mario");
        jumpingExtent = 0;
    }

    public Image doJump() {
        String string;

        this.jumpingExtent++;

        if ( this.jumpingExtent < JUMPING_LIMIT ) {
            if ( mario.getYPosition() > Platform.getInstance().getHeightLimit() )
                mario.setYPosition(mario.getYPosition() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;

            string = mario.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if ( mario.getYPosition() + mario.getHeight() < Platform.getInstance().getFloorOffsetY() ) {
            mario.setYPosition(mario.getYPosition() + 1);
            string = mario.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            string = mario.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            mario.setJumping(false);
            jumpingExtent = 0;
        }

        return Utils.getImage(string);
    }

    private void contact(GameElement gameElement) { //refactored
        if ( gameElement instanceof GameObject ) {
            contactGameObject(gameElement);
        }
        if ( gameElement instanceof EnemyCharacter ) {
            contactEnemyCharacter(gameElement);

        }
    }

    private void contactGameObject(GameElement gameElement) { //lo deve fare il controller
        if ( mario.hitAhead(gameElement) && mario.isToRight() || mario.hitBack(gameElement) && !mario.isToRight() ) {
            Platform.getInstance().setMov(0);
            mario.setMoving(false);
        }

        if ( mario.hitBelow(gameElement) && mario.isJumping() ) {
            Platform.getInstance().setFloorOffsetY(gameElement.getYPosition());
        } else if ( !mario.hitBelow(gameElement) ) {
            Platform.getInstance().setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if ( !mario.isJumping() ) {
                mario.setYPosition(MARIO_OFFSET_Y_INITIAL);
            }

            if ( mario.hitAbove(gameElement) ) {
                Platform.getInstance().setHeightLimit(gameElement.getYPosition() + gameElement.getHeight()); // the new sky goes below the object
            } else if ( !mario.hitAbove(gameElement) && !mario.isJumping() ) {
                Platform.getInstance().setHeightLimit(0); // initial sky
            }

        }
    }

    private void contactEnemyCharacter(GameElement gameElement) {
        EnemyCharacter enemyCharacter = (EnemyCharacter) gameElement;
        if ( mario.hitAhead(enemyCharacter) || mario.hitBack(enemyCharacter) ) {
            if ( enemyCharacter.isAlive() ) {
                mario.setMoving(false);
                mario.setAlive(false);
            } else {
                mario.setAlive(true);
            }
        } else if ( mario.hitBelow(enemyCharacter) ) {
            enemyCharacter.setMoving(false);
            enemyCharacter.setAlive(false);
        }
    }



    public UserCharacter getMario() {
        return mario;
    }

    public void interaction(GameElement gameElement) {
        if ( mario.isNearby(gameElement) ) {
            this.contact(gameElement);
        }
        if ( gameElement instanceof Coin && mario.contactPiece((Coin) gameElement) ){
            Audio.playSound(Res.AUDIO_MONEY);
            coins.remove(gameElement);
        }

    }

    public List<Coin> getCoins() {
        return coins;
    }
}
