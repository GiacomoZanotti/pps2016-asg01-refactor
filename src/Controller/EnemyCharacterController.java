package Controller;

import Model.*;
import Utils.CharacterFactory;
import Utils.MyCharacterFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OPeratore on 19/03/2017.
 */
public class EnemyCharacterController {
    private List<EnemyCharacter> enemies;
    private EnemyCharacter turtle, mushroom;
    private CharacterFactory characterFactory;

    public EnemyCharacterController() {
        characterFactory=new MyCharacterFactory();
        mushroom = characterFactory.createEnemyCharacter(800, 263, "mushroom");
        turtle = characterFactory.createEnemyCharacter(950, 243, "turtle");
        enemies = new ArrayList<>();
        enemies.add(mushroom);
        enemies.add(turtle);
    }


    public List<EnemyCharacter> getEnemies() {
        return enemies;
    }

    public void interaction(GameElement gameElement){
        for(EnemyCharacter enemyCharacter:enemies){
            if(!(enemyCharacter.equals(gameElement))&&!(gameElement instanceof Coin)&&enemyCharacter.isNearby(gameElement)){
                enemyCharacter.contact(gameElement);
            }
        }
    }


}

