package Model;


import java.awt.*;

/**
 * Created by OPeratore on 13/03/2017.
 */
public interface GameCharacter extends GameElement {
    Image walk();

    boolean hitAbove(GameElement gameElement);

    boolean hitAhead(GameElement gameElement);

    boolean hitBack(GameElement gameElement);

    boolean hitBelow(GameElement gameElement);

    boolean isNearby(GameElement gameElement);

    boolean isAlive();

    void setAlive(boolean alive);

    void setXPosition(int xPosition);

    void setYPosition(int yPosition);

    Image deadImage();

    boolean isToRight();

    void setMoving(boolean moving);

    void setToRight(boolean toRight);


}
