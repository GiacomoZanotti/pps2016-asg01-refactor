package Model;

/**
 * Created by OPeratore on 13/03/2017.
 */
public interface EnemyCharacter extends GameCharacter {
    void move();

    void contact(GameElement gameElement);

    int getDeadOffset();

}
