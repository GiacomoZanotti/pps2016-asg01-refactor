package Model;

import Utils.Res;
import Utils.Utils;

public class Tunnel extends GameObject {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 65;

    public Tunnel(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imageGameObject = Utils.getImage(Res.IMG_TUNNEL);
    }

}
