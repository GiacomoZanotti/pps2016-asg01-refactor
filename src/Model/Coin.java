package Model;

import Utils.Res;
import Utils.Utils;

import java.awt.*;

public class Coin extends GameObject implements Runnable{

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int FLIP_FREQUENCY = 100;
    private static final int PAUSE=15;
    private int counter;

    public Coin(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imageGameObject = Utils.getImage(Res.IMG_PIECE1);
    }

    public Image imageOnMovement() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }



    @Override
    public void run() {

            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                System.err.println("something went wrong"+e);
            }

    }

}
