package Model;

import View.Platform;

import java.awt.*;

public  class GameObject implements GameElement {

    private int width, height;
    private int xPosition, yPosition;

    protected Image imageGameObject;

    public GameObject(int x, int y, int width, int height) {
        this.xPosition = x;
        this.yPosition = y;
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getXPosition() {
        return xPosition;
    }

    public int getYPosition() {
        return yPosition;
    }

    public Image getImageGameObject() {
        return imageGameObject;
    }

    public void move() {
        if( Platform.getInstance().getXPosition() >= 0) {
            this.xPosition = this.xPosition -Platform.getInstance().getMov();
        }
    }


}
