package Model;

import java.awt.Image;


import Utils.Res;
import Utils.Utils;

public class Mushroom extends AbstractEnemy  {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;



    public Mushroom(int xPosition, int yPosition) {
        super(xPosition, yPosition, WIDTH, HEIGHT);
        this.setToRight(true);
        this.setMoving(true);


        Thread enemyCharacter = new Thread(this);
        enemyCharacter.start();
    }




    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }

    @Override
    public int getFrequency() {
        return MUSHROOM_FREQUENCY;
    }

    @Override
    public String getImageName() {
        return Res.IMGP_CHARACTER_MUSHROOM;
    }


    @Override
    public int getDeadOffset() {
        return MUSHROOM_DEAD_OFFSET_Y;
    }
}
