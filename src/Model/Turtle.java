package Model;

import java.awt.Image;


import Utils.Res;
import Utils.Utils;

public class Turtle extends AbstractEnemy  {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;


    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
        super.setToRight(true);
        super.setMoving(true);



        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }




    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }

    @Override
    public int getFrequency() {
        return TURTLE_FREQUENCY;
    }

    @Override
    public String getImageName() {
        return Res.IMGP_CHARACTER_TURTLE;
    }

    @Override
    public int getDeadOffset() {
        return  TURTLE_DEAD_OFFSET_Y;
    }
}
