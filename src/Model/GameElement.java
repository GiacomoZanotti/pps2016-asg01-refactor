package Model;

/**
 * Created by OPeratore on 13/03/2017.
 */
public interface GameElement {
    int getWidth();

    int getHeight();

    int getXPosition();

    int getYPosition();

}
