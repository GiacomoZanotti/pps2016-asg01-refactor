package Model;

import java.awt.*;

/**
 * Created by OPeratore on 13/03/2017.
 */
public abstract class AbstractEnemy extends BasicCharacter implements Runnable,EnemyCharacter {
    private int offsetX;
    private final int PAUSE = 15;


    public AbstractEnemy(int xPosition, int yPosition, int width, int height) {
        super(xPosition, yPosition, width, height);
    }

    public void move() {
        this.offsetX = isToRight() ? 1 : -1;
        this.setXPosition(this.getXPosition() + this.offsetX);

    }

    @Override
    public void run() {
        while (this.alive) {

            this.move();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                System.err.println("something went wrong with thread Mush" + e);
            }
        }
    }
    public void contact(GameElement gameElement) {
        if (this.hitAhead(gameElement) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(gameElement) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }


    public abstract Image deadImage();

}
