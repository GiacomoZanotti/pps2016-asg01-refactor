package Model;

import java.awt.Image;


import Utils.Res;
import Utils.Utils;

public abstract class BasicCharacter implements GameCharacter {

    protected static final int PROXIMITY_MARGIN = 10;
    private int width, height;
    protected int xPosition, yPosition;
    protected boolean moving;
    protected boolean toRight;
    protected int counter;
    protected boolean alive;

    public BasicCharacter(int xPosition, int yPosition, int width, int height) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }


    public int getXPosition() {
        return xPosition;
    }

    public int getYPosition() {
        return yPosition;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setXPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public void setYPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public Image walk() {
        String name=this.getImageName();
        String str = Res.IMG_BASE + name + (!this.moving || ++this.counter % this.getFrequency() == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    public boolean hitAbove(GameElement gameElement) {
        return !(this.xPosition + this.width < gameElement.getXPosition() + 5 || this.xPosition > gameElement.getXPosition() + gameElement.getWidth() - 5 ||
                this.yPosition < gameElement.getYPosition() + gameElement.getHeight() || this.yPosition > gameElement.getYPosition() + gameElement.getHeight() + 5);
    }

    public boolean hitAhead(GameElement gameElement) {
        return this.isToRight() && !(this.xPosition + this.width < gameElement.getXPosition() || this.xPosition + this.width > gameElement.getXPosition() + 5 || this.yPosition + this.height <= gameElement.getYPosition() || this.yPosition >= gameElement.getYPosition() + gameElement.getHeight());
    }

    public boolean hitBack(GameElement gameElement) {
        return !(this.xPosition > gameElement.getXPosition() + gameElement.getWidth() || this.xPosition + this.width < gameElement.getXPosition() + gameElement.getWidth() - 5 ||
                this.yPosition + this.height <= gameElement.getYPosition() || this.yPosition >= gameElement.getYPosition() + gameElement.getHeight());
    }

    public boolean hitBelow(GameElement gameElement) {
        return !(this.xPosition + this.width < gameElement.getXPosition() || this.xPosition > gameElement.getXPosition() + gameElement.getWidth() ||
                this.yPosition + this.height < gameElement.getYPosition() || this.yPosition + this.height > gameElement.getYPosition());
    }

    public boolean isNearby(GameElement gameElement) {
        return (this.xPosition > gameElement.getXPosition() - PROXIMITY_MARGIN && this.xPosition < gameElement.getXPosition() + gameElement.getWidth() + PROXIMITY_MARGIN)
                || (this.xPosition + this.width > gameElement.getXPosition() - PROXIMITY_MARGIN && this.xPosition + this.width < gameElement.getXPosition() + gameElement.getWidth() + PROXIMITY_MARGIN);
    }



    public abstract Image deadImage();

    public abstract int getFrequency();

    public abstract String getImageName();


}
