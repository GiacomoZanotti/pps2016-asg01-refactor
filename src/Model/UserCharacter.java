package Model;



/**
 * Created by OPeratore on 13/03/2017.
 */
public interface UserCharacter extends GameCharacter {

    boolean isJumping();
    boolean contactPiece(Coin piece);
    void setJumping(boolean jumping);



}
