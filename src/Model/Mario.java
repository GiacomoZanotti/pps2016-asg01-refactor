package Model;

import java.awt.Image;

import Utils.Res;

public class Mario extends BasicCharacter implements UserCharacter {
    private static final int MARIO_FREQUENCY = 25;
    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;


    private boolean jumping;
    private int jumpingExtent;


    public Mario(int xPosition, int yPosition) {
        super(xPosition, yPosition, WIDTH, HEIGHT);
        this.jumping = false;
        this.jumpingExtent = 0;

    }


    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }


    @Override
    public Image deadImage() {
        return null;
    }

    @Override
    public int getFrequency() {
        return MARIO_FREQUENCY;
    }

    @Override
    public String getImageName() {
        return Res.IMGP_CHARACTER_MARIO;
    }

    public boolean contactPiece(Coin piece) {
        return this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece)
                || this.hitBelow(piece);

    }



}

