package View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;



public class Keyboard implements KeyListener {


    public Keyboard() {

    }

    @Override
    public void keyPressed(KeyEvent e) {
      //  for (UserCharacter userCharacter : controller.getUserCharacters()) {
            if ( Platform.getInstance().getUserCharacter().isAlive() ) {
                if ( e.getKeyCode() == KeyEvent.VK_RIGHT ) {

                    // per non fare muovere il castello e start
                    if ( Platform.getInstance().getXPosition() == -1 ) {
                        Platform.getInstance().setXPosition(0);


                    }
                    Platform.getInstance().getUserCharacter().setMoving(true);
                    Platform.getInstance().getUserCharacter().setToRight(true);
                    Platform.getInstance().setMov(1); // si muove verso sinistra
                } else if ( e.getKeyCode() == KeyEvent.VK_LEFT ) {

                    if ( Platform.getInstance().getXPosition() == 4601 ) {
                        Platform.getInstance().setXPosition(4600);

                    }

                    Platform.getInstance().getUserCharacter().setMoving(true);
                    Platform.getInstance().getUserCharacter().setToRight(false);
                    Platform.getInstance().setMov(-1); // si muove verso destra
                }
                // salto
                if ( e.getKeyCode() == KeyEvent.VK_UP ) {
                    Platform.getInstance().getUserCharacter().setJumping(true);

                    Audio.playSound("/Resources/audio/jump.wav");
                }


        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
            Platform.getInstance().getUserCharacter().setMoving(false);
            Platform.getInstance().setMov(0);

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
