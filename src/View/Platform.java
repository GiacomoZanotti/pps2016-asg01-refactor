package View;

import Model.*;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import Controller.*;

import Utils.ObjectsUtils;
import Utils.Res;
import Utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {


    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;
    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPosition;
    private int floorOffsetY;
    private int heightLimit;
    private Image imgCastle;
    private Image imgFlag;


    private List<GameObject> objects;

    private UserCharacterController userCharacterController;
    private EnemyCharacterController enemyCharacterController;

    private static Platform ourInstance = new Platform();

    public static Platform getInstance() {
        return ourInstance;
    }

    private Platform() {

        initPlatform();

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());

    }

    private void initPlatform() {
        userCharacterController = new UserCharacterController();
        enemyCharacterController =new EnemyCharacterController();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPosition = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);
        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);
        objects = new ArrayList<>(ObjectsUtils.getListOfGameObjects());


    }


    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getXPosition() {
        return xPosition;
    }


    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setXPosition(int xPos) {
        this.xPosition = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public UserCharacter getUserCharacter() {
        return userCharacterController.getMario();
    }


    private void updateBackgroundOnMovement() {
        if ( this.xPosition >= 0 && this.xPosition <= 4600 ) {
            this.xPosition = this.xPosition + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if ( this.background1PosX == -800 ) {
            this.background1PosX = 800;
        } else if ( this.background2PosX == -800 ) {
            this.background2PosX = 800;
        } else if ( this.background1PosX == 800 ) {
            this.background1PosX = -800;
        } else if ( this.background2PosX == 800 ) {
            this.background2PosX = -800;
        }
    }


    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        this.updateBackgroundOnMovement();

//        //game characters interactions with GameObjects and other game characters
        for(GameObject gameObject:objects){
            userCharacterController.interaction(gameObject);
            enemyCharacterController.interaction(gameObject);
        }
        for(Coin coin:userCharacterController.getCoins()){
            userCharacterController.interaction(coin);
        }

        for(EnemyCharacter enemyCharacter:enemyCharacterController.getEnemies()){
            userCharacterController.interaction(enemyCharacter);
            enemyCharacterController.interaction(enemyCharacter);
        }


      // Moving fixed objects
        for (GameObject gameElement : objects) {
            gameElement.move();

        }

        for(Coin coin:userCharacterController.getCoins()){
            coin.move();
        }

        for (EnemyCharacter enemyCharacter : enemyCharacterController.getEnemies()) {
            enemyCharacter.move();
        }
        //Draw steady objects
        drawSteadyObjects(graphics);
        //draw characters
        drawCharacters(graphics);
    }


    private void drawSteadyObjects(Graphics graphics) {

        graphics.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        graphics.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        graphics.drawImage(this.castle, 10 - this.xPosition, 95, null);
        graphics.drawImage(this.start, 220 - this.xPosition, 234, null);

        for (GameObject object : objects) {
            graphics.drawImage(object.getImageGameObject(), object.getXPosition(),
                    object.getYPosition(), null);
        }

        for (Coin piece : userCharacterController.getCoins()) {
            graphics.drawImage(piece.imageOnMovement(), piece.getXPosition(),
                    piece.getYPosition(), null);
        }

        graphics.drawImage(this.imgFlag, FLAG_X_POS - this.xPosition, FLAG_Y_POS, null);
        graphics.drawImage(this.imgCastle, CASTLE_X_POS - this.xPosition, CASTLE_Y_POS, null);
    }

    private void drawCharacters(Graphics graphics) {

        UserCharacter mario = userCharacterController.getMario();
        if ( mario.isJumping() ) {
            graphics.drawImage(userCharacterController.doJump(), mario.getXPosition(), mario.getYPosition(), null);
        } else {
            graphics.drawImage(mario.walk(), mario.getXPosition(), mario.getYPosition(), null);
        }


        for (EnemyCharacter enemyCharacter : enemyCharacterController.getEnemies()) {
            if ( enemyCharacter.isAlive() ) {
                graphics.drawImage(enemyCharacter.walk(), enemyCharacter.getXPosition(), enemyCharacter.getYPosition(), null);
            } else {
                graphics.drawImage(enemyCharacter.deadImage(), enemyCharacter.getXPosition(), enemyCharacter.getYPosition() + enemyCharacter.getDeadOffset(), null);
            }
        }

    }
}



